let $overlayModalUser;
let $modalUser;
let $btnOpenModalUser;
let $btnDisplayLoginPart;
let $modalUserLoginPart;
let $modalUserRegistrationPart;
let $btnDisplayRegistrationPart;
let $btnDisconnect;
let $btnModalCompteButton;
let $btnModalInformationsButton;
let $modalUserComptePart;
let $modalUserInfoPart;


$(document).ready(function() {

    $overlayModalUser = $('#overlayUserPart');
    $modalUser = $('#modalUserPart');
    $btnOpenModalUser = $('#openModalUser');
    $btnDisplayLoginPart = $('#btnDisplayLoginPart');
    $modalUserLoginPart = $('#modal-user-login-part');
    $modalUserRegistrationPart = $('#modal-user-registration-part');
    $btnDisplayRegistrationPart = $('#modalRegistrationButton');
    $btnDisconnect = $('#btn-disconnect');
    $btnModalCompteButton = $('#modalCompteButton');
    $btnModalInformationsButton = $('#modalInformationsButton');
    $modalUserComptePart = $('#modal-user-compte-part');
    $modalUserInfoPart = $('#modal-user-informations-part');

    $btnOpenModalUser.click(openModalUser);

    $overlayModalUser.mousedown(closeModalUser);

    $btnDisplayLoginPart.click(function(e) {
        displayLogin(e);
    });

    $btnDisplayRegistrationPart.click(function(e) {
        displayRegistration(e);
    });

    $btnDisconnect.click(function() {
        user.disconnect();
    })

    $btnModalCompteButton.click(function(e) {
        displayMonCompte(e);
    });

    $btnModalInformationsButton.click(function(e) {
        displayInfo(e);
    });

});

function openModalUser() {
    $overlayModalUser.fadeIn();
}

function closeModalUser(e) {
    if (this === e.target && e.target.id !== "overlay-disable-interaction") {
        $overlayModalUser.fadeOut();
    }
}

function displayRegistration(e, functionExec = null, speed = "easy") {
    $btnDisplayLoginPart.removeClass('active');
    $btnDisplayRegistrationPart.addClass('active');
    $modalUserLoginPart.fadeOut(speed, function() {
        $modalUserRegistrationPart.fadeIn(speed, function() {
            if (functionExec && typeof functionExec === "function") {
                functionExec();
            }
        });
    });
}

function displayLogin(e, functionExec = null, speed = "easy") {
    $btnDisplayRegistrationPart.removeClass('active');
    $btnDisplayLoginPart.addClass('active');
    $modalUserRegistrationPart.fadeOut(speed, function() {
        $modalUserLoginPart.fadeIn(speed, function() {
            if (functionExec && typeof functionExec === "function") {
                functionExec();
            }
        });
    });
}

function displayMonCompte(e, functionExec = null, speed = "easy") {
    $btnModalInformationsButton.removeClass('active');
    $btnModalCompteButton.addClass('active');
    $modalUserInfoPart.fadeOut(speed, function() {
        $modalUserComptePart.fadeIn(speed, function() {
            if (functionExec && typeof functionExec === "function") {
                functionExec();
            }
        });
    });
}

function displayInfo(e, functionExec = null, speed = "easy") {
    $btnModalCompteButton.removeClass('active');
    $btnModalInformationsButton.addClass('active');
    $modalUserComptePart.fadeOut(speed, function() {
        $modalUserInfoPart.fadeIn(speed, function() {
            if (functionExec && typeof functionExec === "function") {
                functionExec();
            }
        });
    });
}

function beforeActionModalUser() {
    disableInteraction();
    activeBtnLoader();
}

function completeActionModalUser() {
    activeInteraction();
    removeBtnLoader();
}