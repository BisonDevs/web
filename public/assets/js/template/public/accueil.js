$(document).ready(function () {
    $("#home-login").click(function (e) {
        displayLogin(e, openModalUser, "fast");
    });

    $("#home-registration").click(function (e) {
        displayRegistration(e, openModalUser, "fast");
    });

    $("#home-modification").click(function () {
        openModalUser();
    });

    $("#home-disconnect").click(function () {
        user.disconnect();
    });
});