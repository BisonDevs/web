$(document).ready(function () {
    $(".menu").click(function (e) {
        $(this).addClass('active');
    });
    $('.menu').mouseleave(function (e) {
        $(this).removeClass('active');
    });

    $('#sendEmailActivationAccount').click(function (){
        $(this).replaceWith('<p style="text-align: center">Un mail vous a été envoyé, recharger la page et réessayer si vous n\'avez rien reçu.</p>');
        user.sendMailActiveAccount();
    });
});