let menuBurger;

$(document).ready(function () {

    $('.subMenu').click(function (e) {
        if (e.target === this || $(e.target).is("span")) {
        changeStateDropdown($(this).find('ul'), $(this));
        }
    });

    menuBurger = new MenuBurger('nav .menu-burger');
    menuBurger.addEventListener(MenuBurger.getChangeStateEvent(), function (){
        if(menuBurger.isActive()){
            $('nav').addClass('active');
        } else {
            $('nav').removeClass('active');
        }
    });
});