// JS
let table;
let typeRessourceModel;

$(document).ready(function () {

    typeRessourceModel = new TypeRessource();
    table = new Table('table-admin-types');
    // Premier filtrage
    filterResourcesTypes();
});

/**
 * Permet de filtrer les ressources
 *
 */
function filterResourcesTypes() {

    // Paramètres envoyés pour la requête api
    let params = {};
    params[Model.LOADER] = "global-loader";

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        table.bindTable(data, createRowTypeRessource);
    };
    // Récupération de la liste des catégories ressources
    typeRessourceModel.callAction(Model.LIST, params);
}

function createRowTypeRessource(data) {
    let tr = document.createElement('tr');

    let tdLibelle = document.createElement('td');
    let lib = data.libelle;
    tdLibelle.innerText = lib;
    tdLibelle.title = lib;
    tr.append(tdLibelle);

    let tdCreation = document.createElement('td');
    let createdDate = parseDateToFr(data.createdDate);
    tdCreation.innerText = createdDate;
    tdCreation.title = createdDate;
    tr.append(tdCreation);

    let tdUpdate = document.createElement('td');
    if (data.updatedDate) {
        let updatedDate = parseDateToFr(data.updatedDate);
        tdUpdate.innerText = updatedDate;
        tdUpdate.title = updatedDate;
    }
    tr.append(tdUpdate);

    let tdAction = document.createElement('td');
    let divAction = document.createElement('div');
    divAction.className = "divAction";
    let $iEdit = $('<i class="fas fa-edit"></i>');
    $iEdit.click(displayNonFait);
    $(divAction).append($iEdit);
    tdAction.append(divAction);

    tr.append(tdAction);

    return tr;
}