window.Apex = {
    chart: {
        foreColor: '#ccc',
        toolbar: {
            show: false
        },
    },
    stroke: {
        width: 3
    },
    dataLabels: {
        enabled: false
    },
    tooltip: {
        theme: 'dark'
    },
    grid: {
        borderColor: "#535A6C",
        xaxis: {
            lines: {
                show: true
            }
        }
    }
};

var spark1 = {
    chart: {
        id: 'spark1',
        group: 'sparks',
        type: 'line',
        height: 80,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [25, 66, 41, 59, 25, 44, 12, 36, 9, 21]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    grid: {
        padding: {
            top: 20,
            bottom: 10,
            left: 110
        }
    },
    colors: ['#fff'],
    tooltip: {
        x: {
            show: false
        },
        y: {
            title: {
                formatter: function formatter(val) {
                    return '';
                }
            }
        }
    }
}

var spark2 = {
    chart: {
        id: 'spark2',
        group: 'sparks',
        type: 'line',
        height: 80,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [12, 14, 2, 47, 32, 44, 14, 55, 41, 69]
    }],
    stroke: {
        curve: 'smooth'
    },
    grid: {
        padding: {
            top: 20,
            bottom: 10,
            left: 110
        }
    },
    markers: {
        size: 0
    },
    colors: ['#fff'],
    tooltip: {
        x: {
            show: false
        },
        y: {
            title: {
                formatter: function formatter(val) {
                    return '';
                }
            }
        }
    }
}

var spark3 = {
    chart: {
        id: 'spark3',
        group: 'sparks',
        type: 'line',
        height: 80,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [47, 45, 74, 32, 56, 31, 44, 33, 45, 19]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    grid: {
        padding: {
            top: 20,
            bottom: 10,
            left: 110
        }
    },
    colors: ['#fff'],
    xaxis: {
        crosshairs: {
            width: 1
        },
    },
    tooltip: {
        x: {
            show: false
        },
        y: {
            title: {
                formatter: function formatter(val) {
                    return '';
                }
            }
        }
    }
}

var spark4 = {
    chart: {
        id: 'spark4',
        group: 'sparks',
        type: 'line',
        height: 80,
        sparkline: {
            enabled: true
        },
        dropShadow: {
            enabled: true,
            top: 1,
            left: 1,
            blur: 2,
            opacity: 0.2,
        }
    },
    series: [{
        data: [15, 75, 47, 65, 14, 32, 19, 54, 44, 61]
    }],
    stroke: {
        curve: 'smooth'
    },
    markers: {
        size: 0
    },
    grid: {
        padding: {
            top: 20,
            bottom: 10,
            left: 110
        }
    },
    colors: ['#fff'],
    xaxis: {
        crosshairs: {
            width: 1
        },
    },
    tooltip: {
        x: {
            show: false
        },
        y: {
            title: {
                formatter: function formatter(val) {
                    return '';
                }
            }
        }
    }
}

new ApexCharts(document.querySelector("#spark1"), spark1).render();
new ApexCharts(document.querySelector("#spark2"), spark2).render();
new ApexCharts(document.querySelector("#spark3"), spark3).render();
new ApexCharts(document.querySelector("#spark4"), spark4).render();


var optionsLine = {
    chart: {
        height: 328,
        type: 'line',
        zoom: {
            enabled: false
        },
        dropShadow: {
            enabled: true,
            top: 3,
            left: 2,
            blur: 4,
            opacity: 1,
        }
    },
    stroke: {
        curve: 'smooth',
        width: 2
    },
    //colors: ["#3F51B5", '#2196F3'],
    series: [
        {
        name: "Refusée",
        data: [1, 15, 26, 20, 33, 27, 53, 20]
    },
        {
            name: "Validée",
            data: [3, 33, 21, 42, 19, 32, 10, 48]
        },
        {
            name: "En attente",
            data: [0, 39, 52, 11, 29, 43, 23, 78]
        }
    ],
    title: {
        text: 'Nombre de relation par personne moyen',
        align: 'left',
        offsetY: 25,
        offsetX: 20
    },
    markers: {
        size: 6,
        strokeWidth: 0,
        hover: {
            size: 9
        }
    },
    grid: {
        show: true,
        padding: {
            bottom: 0
        }
    },
    labels: ['07/20', '08/20', '09/20', '10/20', '11/20', '12/20', '01/21', '02/21'],
    xaxis: {
        tooltip: {
            enabled: false
        }
    },
    legend: {
        position: 'top',
        horizontalAlign: 'right',
        offsetY: -20
    }
}

var chartLine = new ApexCharts(document.querySelector('#line-adwords'), optionsLine);
chartLine.render();

var optionsCircle4 = {
    title: {
        text: 'Nombre d\'utilisateur inscrit par jour',
        align: 'left',
        offsetY: 25,
        offsetX: 20
    },
    chart: {
        type: 'radialBar',
        height: 350,
        width: 380,
    },
    plotOptions: {
        radialBar: {
            size: undefined,
            inverseOrder: true,
            hollow: {
                margin: 5,
                size: '48%',
                background: 'transparent',

            },
            track: {
                show: false,
            },
            startAngle: -180,
            endAngle: 180

        },
    },
    stroke: {
        lineCap: 'round'
    },
    series: [71, 63, 77],
    labels: ['Avril', 'Mai', 'Juin'],
    legend: {
        show: true,
        floating: true,
        position: 'right',
        offsetX: 70,
        offsetY: 240
    },
}

var chartCircle4 = new ApexCharts(document.querySelector('#radialBarBottom'), optionsCircle4);
chartCircle4.render();


var optionsBar = {
    chart: {
        height: 380,
        type: 'bar',
        stacked: true,
    },
    plotOptions: {
        bar: {
            columnWidth: '30%',
            horizontal: false,
        },
    },
    series: [{
        name: 'Activité/Jeu à réaliser',
        data: [14, 25, 21, 17, 12, 13, 11, 19]
    }, {
        name: 'Article',
        data: [13, 23, 20, 8, 13, 27, 33, 12]
    }, {
        name: 'Carte Défi',
        data: [11, 17, 15, 15, 21, 14, 15, 13]
    }, {
        name: 'Exercice/Atelier',
        data: [10, 8, 12, 15, 12, 60, 40, 25]
    }, {
        name: 'Fiche de lecture',
        data: [2, 11, 0, 21, 3, 32, 12, 33]
    }, {
        name: 'Jeu en ligne',
        data: [9, 15, 50, 10, 0, 2, 13, 1]
    }, {
        name: 'Vidéo',
        data: [111, 50, 32, 36, 76, 25, 31, 0]
    }],
    xaxis: {
        categories: ['07/20', '08/20', '09/20', '10/20', '11/20', '12/20', '01/21', '02/21'],
    },
    fill: {
        opacity: 1
    },

}

var chartBar = new ApexCharts(
    document.querySelector("#barchart"),
    optionsBar
);

chartBar.render();

var optionsArea = {
    chart: {
        height: 380,
        type: 'area',
        stacked: false,
    },
    stroke: {
        curve: 'straight'
    },
    series: [
        {
        name: "Partagées",
        data: [11, 15, 26, 20, 33, 27, 35, 48]
    },
        {
            name: "Mises de côté",
            data: [32, 33, 21, 42, 19, 32, 27, 44]
        },
        {
            name: "Favorites",
            data: [20, 39, 52, 11, 29, 43, 45, 38]
        }
    ],
    xaxis: {
        categories: ['07/20', '08/20', '09/20', '10/20', '11/20', '12/20', '01/21', '02/21'],
    },
    tooltip: {
        followCursor: true
    },
    fill: {
        opacity: 1,
    },

}

var chartArea = new ApexCharts(
    document.querySelector("#areachart"),
    optionsArea
);

chartArea.render();

/* fusionexport integrations START */
(() => {
        const btn = document.getElementById('fusionexport-btn')
        btn.addEventListener('click', async function () {
            const endPoint = 'https://www.fusioncharts.com/demos/dashboards/fusionexport-apexcharts/api/export-dashboard'
            const information = {
                dashboardName: 'dark'
            };

            this.setAttribute('disabled', true);
            const {data} = await axios.post(endPoint, information, {
                responseType: 'blob'
            });
            await download(data, 'apexCharts-dark-dashboard.pdf', 'application/pdf')
            this.removeAttribute('disabled')
        });
    }
)();
/* fusionexport integrations END */