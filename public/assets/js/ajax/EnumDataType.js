class EnumDataType {

    static JSON = "json";
    static JSON_LD = "ld+json";
    static TEXT = "text";
    static XML = "xml";

}
