class MultipleSelect {
    id;
    // method;
    // parameter;

    initMultipleSelect(parameter) {
        $('#' + this.id).multipleSelect(parameter);
    }

    close(){
        $('#' + this.id).multipleSelect('close');
    }

    open(){
        $('#' + this.id).multipleSelect('open');
    }
}