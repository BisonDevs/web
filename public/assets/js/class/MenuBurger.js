class MenuBurger extends EventTarget {

    static classEl = "change";
    $element;

    constructor(selector) {
        super();
        this.$element = $(selector);
        let obj = this;
        this.$element.click(function () {
            if ($(this).hasClass(MenuBurger.classEl)) {
                $(this).removeClass(MenuBurger.classEl);
            } else {
                $(this).addClass(MenuBurger.classEl);
            }
            obj.changeStateEl();
        });
        this.changeState = new CustomEvent(MenuBurger.getChangeStateEvent());
    }

    changeStateEl() {
        this.dispatchEvent(this.changeState);
    }

    isActive() {
        return this.$element.hasClass(MenuBurger.classEl);
    }

    static getChangeStateEvent() {
        return 'menu-burger-change-state';
    }

}