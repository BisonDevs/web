class RessourceModel extends Model {

    #urlRessource = apiUrl + "/ressources";
    slug;
    titre;
    contenu;
    categorieRessource;
    typeRessource;
    createur;
    lsCommentaires;
    etatRessourceUtilisateurs;
    lsPartages;
    lsDiscussions;
    createdDate;
    updatedDate;
    activatedDate;
    deletedDate;
    image;

    actionAvailableForObject() {
        let obj = {};

        obj[Model.GET] = {};
        obj[Model.GET][Model.AVAILABLE] = true;
        obj[Model.GET][Model.AUTH] = false;

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = true;

        obj[Model.PUT] = {};
        obj[Model.PUT][Model.AVAILABLE] = true;
        obj[Model.PUT][Model.AUTH] = true;

        obj[Model.PATCH] = {};
        obj[Model.PATCH][Model.AVAILABLE] = true;
        obj[Model.PATCH][Model.AUTH] = true;

        obj[Model.LIST] = {};
        obj[Model.LIST][Model.AVAILABLE] = true;
        obj[Model.LIST][Model.AUTH] = false;

        obj[Model.DELETE] = {};
        obj[Model.DELETE][Model.AVAILABLE] = true;
        obj[Model.DELETE][Model.AUTH] = true;

        return obj;
    }

    constructor() {
        super("ressources");
    }

    filterResources(
        successFunction, page = 1, nbPerPage = 10,
        lsTypeRelationFiltre = null, lsTypeRessourceFiltre = null,
        lsCategorieRessourceFiltre = null, userFiltre = null,
        titleFiltre = null, orderBy = null,
        loader = "global-loader") {

        let params = {};
        params[Model.LOADER] = loader;
        // Fonction exécutée au succès de la requête ajax permettant de récupérer les ressources
        params[Model.SUCCESS_FUNC] = successFunction;

        params[Model.URL_PARAMTERS] = {
            "page": page,
            "itemsPerPage": nbPerPage,
        };

        if (lsTypeRelationFiltre !== null && lsTypeRelationFiltre !== "") {
            let indexTypeRelation = "custom_type_relation_filter[]";
            if (lsTypeRelationFiltre.length === 0) {
                params[Model.URL_PARAMTERS][indexTypeRelation] = "";
            } else {
                params[Model.URL_PARAMTERS][indexTypeRelation] = [];
                for (let idTypeRelation of lsTypeRelationFiltre) {
                    params[Model.URL_PARAMTERS][indexTypeRelation].push(idTypeRelation);
                }
            }
        }

        if (lsTypeRessourceFiltre !== null && lsTypeRessourceFiltre !== "") {
            let indexTypeRessource = "typeRessource.slug[]";
            if (lsTypeRessourceFiltre.length === 0) {
                params[Model.URL_PARAMTERS][indexTypeRessource] = "";
            } else {
                params[Model.URL_PARAMTERS][indexTypeRessource] = [];
                for (let idTypeRessource of lsTypeRessourceFiltre) {
                    params[Model.URL_PARAMTERS][indexTypeRessource].push(idTypeRessource);
                }
            }
        }

        if (lsCategorieRessourceFiltre !== null && lsCategorieRessourceFiltre !== "") {
            let indexCategorieRessource = "categorieRessource.slug[]";
            if (lsCategorieRessourceFiltre.length === 0) {
                params[Model.URL_PARAMTERS][indexCategorieRessource] = "";
            } else {
                params[Model.URL_PARAMTERS][indexCategorieRessource] = [];
                for (let idCategRessource of lsCategorieRessourceFiltre) {
                    params[Model.URL_PARAMTERS][indexCategorieRessource].push(idCategRessource);
                }
            }
        }

        if (userFiltre) {
            let indexUser = "custom_user_filter";
            params[Model.URL_PARAMTERS][indexUser] = userFiltre;
        }

        if (titleFiltre) {
            params[Model.URL_PARAMTERS]['titre'] = titleFiltre;
        }

        // if (orderBy) {
        //     params[Model.URL_PARAMTERS]["order[" + orderBy + "]"] = "asc";
        // }

        if (user.connection && user.connection.tokenObject &&
            (user.connection.tokenObject.refresh_token || user.connection.tokenObject.getToken())
        ) {
            params[Model.AUTH] = true;
        }
        this.callAction(Model.LIST, params);

    }

    toSummaryDiv() {

        // Div contenant la ressource
        let divSummaryResource = document.createElement('div');
        divSummaryResource.className = "summary-resource";

        // Div contenant le titre, l'image et le créateur
        let divImageTitle = document.createElement('div');
        divImageTitle.className = "div-title-image";

        // Element titre
        let title = document.createElement('h3');
        title.innerText = capitalize(this.categorieRessource.libelle) + " : " + capitalize(this.titre);

        // Div contenant l'image
        let divImage = document.createElement('div');
        divImage.className = "div-action-image-summary-resource";

        let image;
        if (this.image) {
            // Si une image est renseignée on l'affiche dans une balise img
            image = document.createElement('img');
            image.setAttribute('src', this.image.filePath);
            image.setAttribute('alt', this.titre);
        } else {
            // Sinon on créé une fausse image  dans une div
            image = document.createElement('div');
            let i = document.createElement('i');
            i.className = "far fa-8x fa-file-image";
            let noImage = document.createElement('div');
            noImage.innerText = "Aucune image disponible";
            noImage.className = "div-no-image-summary-resource";
            image.append(i);
            image.append(noImage);
        }
        image.className = "div-image-summary-resource";

        // Div contenant la liste des actions à faire (favoris, utilisé, ...)
        let divResourceAction = document.createElement('div');
        divResourceAction.className = "div-action-summary-resource";

        let iconFavorit = document.createElement('i');
        iconFavorit.className = "far fa-star";
        $(iconFavorit).click(displayNonFait);

        let iconDeCote = document.createElement('i');
        iconDeCote.className = "far fa-folder";
        $(iconDeCote).click(displayNonFait);

        let iconUse = document.createElement('i');
        iconUse.className = "far fa-check-circle";
        $(iconUse).click(displayNonFait);

        let iconFollow = document.createElement('i');
        iconFollow.className = "far fa-share-square";
        $(iconFollow).click(displayNonFait);

        // On ajoute les différents icones
        divResourceAction.append(iconFavorit);
        divResourceAction.append(iconDeCote);
        divResourceAction.append(iconUse);
        divResourceAction.append(iconFollow);

        // On ajoute l'image et les actions
        divImage.append(image);
        divImage.append(divResourceAction);

        // Div contenant le nom du créateur et le type de ressource
        let divNameTypeResource = document.createElement('div');
        divNameTypeResource.className = "div-name-type-resource";

        let divName = document.createElement('span');
        divName.innerText = this.getCreatorName();

        let divColorTypeResource = document.createElement('div');
        divColorTypeResource.className = "block-color " + this.typeRessource.slug;
        divColorTypeResource.title = this.typeRessource.libelle;

        divNameTypeResource.append(divName);
        divNameTypeResource.append(divColorTypeResource);

        let buttonSeeResource = document.createElement('a');
        buttonSeeResource.setAttribute('href', '/ressources/' + this.slug);
        buttonSeeResource.innerText = 'Consulter';
        buttonSeeResource.className = 'btn-info';

        divImageTitle.append(title);
        divImageTitle.append(divImage);
        divImageTitle.append(divNameTypeResource);
        divSummaryResource.append(divImageTitle);
        divSummaryResource.append(buttonSeeResource);

        return divSummaryResource;
    }

    getCreatorName() {
        let name = "";

        if (this.createur) {
            if (this.createur.pseudo) {
                name = capitalize(this.createur.pseudo);
            } else if (this.createur.nom && this.createur.prenom) {
                name = this.createur.nom.toUpperCase() + " " + capitalize(this.createur.prenom);
            }
        }

        return name;
    }
}