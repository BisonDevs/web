class Message extends Model {
    contenu;
    auteur;
    discussion;
    messageParent;
    messageEnfants;
    createdDate;
    updatedDate;
    deletedDate;

    actionAvailableForObject() {
        let obj = {};

        obj[Model.GET] = {};
        obj[Model.GET][Model.AVAILABLE] = true;
        obj[Model.GET][Model.AUTH] = false;

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = true;

        obj[Model.LIST] = {};
        obj[Model.LIST][Model.AVAILABLE] = true;
        obj[Model.LIST][Model.AUTH] = false;

        obj[Model.DELETE] = {};
        obj[Model.DELETE][Model.AVAILABLE] = true;
        obj[Model.DELETE][Model.AUTH] = true;

        return obj;
    }

    constructor() {
        super("messages");
    }
}
