class Table {

    $tableResource;

    constructor(id) {
        this.$tableResource = $('#' + id);
    }

    bindTable(data, functionCreateRow) {

        let $body = this.$tableResource.find('tbody');

        // Vide le contenu de la page
        $body.html("");

        // Récupération de la liste de ressource
        data = Model.getListFromJSONLD(data);

        for (let index in data) {
            let line = data[index];
            $body.append(functionCreateRow(line));
        }
    }

}