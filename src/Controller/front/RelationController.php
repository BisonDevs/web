<?php


namespace App\Controller\front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RelationController
 * @package App\Controller\front
 * @Route("/relations", name="relation_")
 */
class RelationController extends AbstractController
{
    /**
     * @Route(name="page")
     */
    public function index(): Response
    {
        return $this->render('/public/relations/index.html.twig');
    }
}