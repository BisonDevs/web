<?php


namespace App\Controller\front;


use App\Repository\ResourceRepository;
use App\Repository\RessourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\front
 * @Route("/ressources", name="resource_")
 */
class RessourceController extends AbstractController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('public/ressources/list.html.twig', []);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('public/ressources/form.html.twig', ["multipleSelect" => true, "tiny" => true, "dropzone" => true]);
    }

    /**
     * @Route("/modification/{id}", name="update")
     * @param String $id
     * @param ResourceRepository $repository
     * @return Response
     */
    public function update(string $id, ResourceRepository $repository): Response
    {
        try {
            $ressource = $repository->getResource($id);

            return $this->render('public/ressources/form.html.twig', [
                "multipleSelect" => true,
                "tiny" => true,
                "dropzone" => true,
                "ressource" => $ressource
            ]);
        } catch (\Exception $e) {

        }

        return $this->render('public/404.html.twig');

    }

    /**
     * @Route("/mes_ressources", name="my_resources")
     */
    public function my_ressources(): Response
    {
        return $this->render('public/ressources/my_resources.html.twig');
    }

    /**
     * @Route("/{id}", name="show")
     * @param String $id
     * @param ResourceRepository $repository
     * @return Response
     */
    public function show_ressource(string $id, ResourceRepository $repository): Response
    {
        try {
            $ressource = $repository->getResource($id);

            return $this->render('public/ressources/ressource.html.twig', [
                "ressource" => $ressource
            ]);
        } catch (\Exception $e) {
            echo "<pre>";
            var_dump($ressource ?? null);
            var_dump($e->getMessage());
            var_dump($e->getTraceAsString());

        }

        return $this->render('public/404.html.twig');
    }

}