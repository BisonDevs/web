<?php

namespace App\Controller\front;

use App\Enumeration\HTTPMethod;
use App\Service\Curl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\front
 * @Route("utilisateur", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/connexion", name="login")
     */
    public function login(): Response
    {
        if (!empty($this->getUser())) {
            return $this->redirectToRoute("user_account");
        } else {
            return $this->render('public/user/form.html.twig', [
                'form' => 'login',
            ]);
        }
    }

    /**
     * @Route("/inscription", name="registration")
     */
    public function registration(): Response
    {
        if (!empty($this->getUser())) {
            return $this->redirectToRoute("user_account");
        } else {
            return $this->render('public/user/form.html.twig', [
                'form' => 'registration',
            ]);
        }
    }

    /**
     * @Route("/desinscription", name="unsubscribe")
     */
    public function unsubscribe(): Response
    {
        return $this->render('public/user/form.html.twig', [
            'form' => 'unsubscribe',
        ]);
    }

    /**
     * @Route("/mon-compte", name="account")
     */
    public function compte(): Response
    {
        return $this->render('public/user/mon-compte.html.twig', [
            'form' => 'compte',
        ]);
    }

    /**
     * @Route("/mon-compte/valide", name="valide")
     */
    public function valide(): Response
    {
        return $this->render('public/user/valide.html.twig', [
            'form' => 'valide',
        ]);
    }


    /**
     * @param string $token
     * @return Response
     * @Route("/active/{token}", name="active")
     */
    public function active(string $token): Response
    {
        $url = $_SERVER['API_HTTP'] . '/api/users/' . $token . '/active_compte';
        $method = HTTPMethod::GET;

        $curl = new Curl($url, $method);
        $res = $curl->execute();

        return $this->render('public/user/active.html.twig', [
            'res' => json_decode($res)
        ]);
    }

}


