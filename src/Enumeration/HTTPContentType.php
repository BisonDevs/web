<?php


namespace App\Enumeration;


class HTTPContentType
{
    public const JSON_LD = "application/ld+json";
    public const JSON = "application/json";
    public const HTML = "text/html";
}