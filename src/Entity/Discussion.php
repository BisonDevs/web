<?php

namespace App\Entity;

use DateTime;

class Discussion extends Entity
{

    protected String $slug;

    protected String $titre;

    /**
     * @var User|String
     */
    protected $createur;

    protected array $participants = [];

    /**
     * @var Ressource|String
     */
    protected $ressourceSupport;

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    protected DateTime $deletedDate;

    /**
     * @return String
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return String
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return User|String
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * @return array
     */
    public function getParticipants(): array
    {
        return $this->participants;
    }

    /**
     * @return Ressource|String
     */
    public function getRessourceSupport()
    {
        return $this->ressourceSupport;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return DateTime
     */
    public function getDeletedDate(): DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param String $titre
     */
    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @param User|String $createur
     */
    public function setCreateur($createur): void
    {
        $this->createur = $createur;
    }

    /**
     * @param array $participants
     */
    public function setParticipants(array $participants): void
    {
        $this->participants = $participants;
    }

    /**
     * @param Ressource|String $ressourceSupport
     */
    public function setRessourceSupport($ressourceSupport): void
    {
        $this->ressourceSupport = $ressourceSupport;
    }


}
