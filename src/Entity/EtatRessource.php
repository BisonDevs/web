<?php

namespace App\Entity;

use DateTime;

class EtatRessource extends Entity
{

    protected String $slug;

    protected String $libelle;

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    protected DateTime $deletedDate;

    /**
     * @return String
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return String
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return DateTime
     */
    public function getDeletedDate(): DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param String $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

}
