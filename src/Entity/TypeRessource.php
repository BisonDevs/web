<?php

namespace App\Entity;

use DateTime;

class TypeRessource
{
    protected String $slug;

    protected String $libelle;

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    /**
     * @var MediaObject|String
     */
    private $logo;

    /**
     * @return String
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return String
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return MediaObject|String
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param String $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @param MediaObject|String $logo
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }


}
